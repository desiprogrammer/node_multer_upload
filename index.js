const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const multer = require('multer');
const bodyparser = require('body-parser');

// The App 
const app = express();

// Setting Up View Engine And Static Directory 
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('public'))

// Connecting To The Database
// Use Your Own DB Connection String
mongoose.connect('mongodb+srv://prince:tododb@cluster0-d7fvf.mongodb.net/test01?retryWrites=true&w=majority', {
    useNewUrlParser: true, useUnifiedTopology: true,
}).then(() => console.log("Database Connected")
);

// A simple schema 
var userschema = new mongoose.Schema({
    username : {
        type : String,
    },
    fileurl : {
        type : String,
    }
});

// the model to interact with the schema 
var usermodel = new mongoose.model('usermodel', userschema);

// setting up multer diskstorage and name
const storage = multer.diskStorage({
    destination : './public/uploads',
    // function to set the name of file that will be saved in disk 
    filename : function(req, file, cb){
        cb(null, req.body.username + Date.now() + path.extname(file.originalname));
    }
});

// the upload method 
const upload = multer({
    storage : storage,
    // filter file extensions 
    fileFilter : function(req, file, callback){
        var ext = path.extname(file.originalname);
        if(ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg'){
            return callback(new Error("Only Images Are Allowed..!"));
        }
        callback(null, true)
    },
    limits : { fileSize : 100 }
}).single('file');

// Routes
app.get('/', (req, res) => {
    usermodel.find({}, (err, data) => {
        if (err) throw err;
        res.render('index', { 'data' : data });
    });
});

// The upload route where files are uploaded 
app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            res.render('index', { 'err' : err });
        }else{
            var file_url = 'uploads/' + req.file.filename;
            var username = req.body.username;
            usermodel({
                username,
                fileurl : file_url,
            }).save((err, data) => {
                if (err) throw err;
                res.redirect('/');
            });
        }
    });
});

// The Port and app listening to port 
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log("Server Started...");
});