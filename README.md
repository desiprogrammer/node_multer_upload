# Node File/Image Upload Using Multer

## [Tutorial](https://youtu.be/bxRZX5sgMiQ)

## Code Blocks
### Setting Up The Disk Storage
````javascript
    const storage = multer.diskStorage({
    destination : './public/uploads',
    // function to set the name of file that will be saved in disk 
    filename : function(req, file, cb){
        cb(null, req.body.username + Date.now() + path.extname(file.originalname));
    }
});
````

### Upload Method For Single File Upload
````javascript
    const upload = multer({
    storage : storage,
    // filter file extensions 
    fileFilter : function(req, file, callback){
        var ext = path.extname(file.originalname);
        if(ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg'){
            return callback(new Error("Only Images Are Allowed..!"));
        }
        callback(null, true)
    },
    limits : { fileSize : 100 } // (Optional Size (in bytes))
}).single('file');
});
````

### The POST Route for Uploading
````javascript
    app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            res.render('index', { 'err' : err });
        }else{
            var file_url = 'uploads/' + req.file.filename; // gives file URL in Disk
            // Optional -- According to My Project
            var username = req.body.username;
            usermodel({
                username,
                fileurl : file_url,
            }).save((err, data) => {
                if (err) throw err;
                res.redirect('/');
            });
        }
    });
});
````

## To Run This Project

`````cmd
    npm install
    npm start (or) npm run dev
`````